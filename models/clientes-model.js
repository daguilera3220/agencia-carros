'use strict'
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var ClientesSchema = Schema({
    firstName:String,
    lastName: String,
    phone:String,
    car:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'carros'
    }
});

ClientesSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('clientes', ClientesSchema);