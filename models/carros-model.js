'use strict'
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var CarrosSchema = Schema({
    name:String,
    year: Number,
    description:String,
    deleted: Boolean
});

CarrosSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('carros', CarrosSchema);