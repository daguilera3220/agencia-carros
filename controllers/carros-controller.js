var Cars = require('../models/carros-model');

function home(req,res){
    res.status(200).render('home', {title:"Home"});
}

function readCreateCar(req, res){
    var currentTime = new Date();
    var year = currentTime.getFullYear();
    res.status(200).render('cars/create', {title: "Add new car", year: year });
}

function createCar(req,res){
    var car = new Cars();

    var params = req.body;

    car.name = params.name;
    car.year = params.year;
    car.description = params.description;

    car.save((err, guardado) => {

        if(err){
            res.status(500).send({message:"Saving error"})
        }

        res.redirect('../list/1');
    });
}

function readCar(req, res){
    var pg = req.params.pg;
    Cars.paginate({deleted: null},{ page: pg, limit: 6 }, function(err,posts){
        if(err){
            res.status(500).send({message:"ERROR al regresar usuarios"})
        }
        if(!posts){
            res.status(404).send({message:"No se encontro ningun dato m8"})
        }
    }).then(response => {
        res.status(200).render('cars/list',
            {title: "Cars List", cars: response.docs, pagina: response.pages, active: response.page})
    });
}

function readUpdateCar(req, res){
    var carId = req.params.id;
    var currentTime = new Date();
    var year = currentTime.getFullYear();
    Cars.findById(carId, function (err, posts) {
        if (!posts){
            res.status(500).send("error at showing car");
        }
        res.status(200).render('cars/update', {title: "Update car specs", car: posts, year:year})
    });
}

function updateCar(req, res){
    var carId = req.params.id;
    var updateInfo = req.body;
    Cars.findByIdAndUpdate(carId, updateInfo, function (err, car) {
        if(!car){
            res.status(404).send("We couldnt find the car");
        }
        if (err){
            res.status(500).send("Server error");
        }
        res.redirect('../list/1');
    });
}

function deleteCar(req,res){
    var carId = req.params.id;
    Cars.findByIdAndUpdate(
        carId,
        {deleted: true},
        {multi: true},
        function (err) {
            if (err) {
            res.status(500).send("Server Error");
            }
        res.redirect('../list/1');
    });
}

module.exports = {
    readCar,
    updateCar,
    deleteCar,
    readUpdateCar,
    readCreateCar,
    createCar,
    home
};