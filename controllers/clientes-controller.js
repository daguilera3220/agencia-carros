var Clients = require('../models/clientes-model');
var Cars = require('../models/carros-model');

function readCreateClient(req, res){
    Cars.find({deleted: null}, function(err, cars){
        if(err) {
            res.status(500).send({message: "Error at showing cars"})
        }
        if(!cars){
            res.status(404).send({message:"no cars found"})
        }
        res.status(200).render('clients/create', {title: "Add new client", cars: cars});
    });
}

function createClient(req,res){
    var client = new Clients();

    var params = req.body;

    client.firstName = params.firstName;
    client.lastName = params.lastName;
    client.phone = params.phone;
    Cars.findById(req.body.car, function (err, post) {
        if (!post){
            res.status(500).send("error at showing car");
        }
        client.car = post;
        client.save((err, guardado) => {

            if(err){
                res.status(500).send({message:"Saving error"})
            }
            res.redirect('../list/1');
        });
    });
}

function readClient(req, res){
    var pg = req.params.pg;
    Clients.paginate({},{ page: pg, limit: 6, populate: 'car' }, function(err,posts){
        if(err){
            res.status(500).send({message:"ERROR al regresar clientes"})
        }
        if(!posts){
            res.status(404).send({message:"Error Server"})
        }
    }).then(response => {
        console.log(response.docs);
        res.status(200).render('clients/list',
            {title: "Clients List", clients: response.docs, pagina: response.pages, active: response.page})
    });

    // Clients.find({}).populate('car').exec(function (err, posts) {
    //         if (err) {
    //             res.status(500).send({message: "Error at showing clients"})
    //         }
    //         if (!posts) {
    //             res.status(404).send({message: "no files found"})
    //         }
    //         console.log(posts);
    //         res.status(200).render('clients/list', {title: "Clients List", clients: posts})
    //     });
};

function readUpdateClient(req, res){
    var cId = req.params.id;
    Clients.findById(cId).exec(
        function (err, posts) {
            if (!posts){
                res.status(500).send("error at showing client");
            }
            Cars.find({deleted : null}, function(err,car){
                console.log(posts)
                res.status(200).render('clients/update', {title: "Update client", client: posts, cars: car})
            })
        });
}

function updateClient(req, res){
    var cId = req.params.id;
    var updateInfo = req.body;
    console.log(updateInfo);
    Clients.findByIdAndUpdate(cId, updateInfo, function (err, car) {
        if (err){
            res.status(500).send("Server error");
        }
        res.redirect('../list/1');
    });
}

function deleteClient(req,res){
    var cId = req.params.id;
    Clients.findOneAndRemove({_id: cId}, function (err) {
        if (err) {
            res.status(500).send("Server Error");
        }
        res.redirect('../list/1');
    });
}

module.exports = {
    readClient,
    updateClient,
    deleteClient,
    readUpdateClient,
    readCreateClient,
    createClient
};