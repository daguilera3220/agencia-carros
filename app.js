'use strict'
//Aqui va toda la configuracion de express, se cargan todas las rutas, body parser

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var carros = require('./routes/carros-routes');
var clientes = require('./routes/clientes-routes');
var home = require('./routes/home-routes');

app.set("view engine", "pug");
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//todas las rutas van aqui
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/node_modules/bootstrap/dist'));
app.use(express.static(__dirname + '/node_modules/jquery/dist'));
app.use(express.static(__dirname + '/node_modules/material-design-iconic-font/dist'));
app.use('/car', carros);
app.use('/client', clientes);
app.use('/', home)

module.exports = app;
