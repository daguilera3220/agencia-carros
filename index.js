'use strict'
//se carga archivo app.js
var app = require('./app');
const mongoose = require('mongoose');
const CONNECTION_URI = 'mongodb://admin:12345@ds119640.mlab.com:19640/agenciacarros';
// const CONNECTION_URI = 'mongodb://localhost:27017/api_rest';

const PORT = process.env.PORT || 3000;

mongoose.connect(CONNECTION_URI, (err,res) => {
    if(err){
        throw err;
    }else{
        console.log("Conexion a mongo exitosa");
        app.listen(PORT, function ()  {
            console.log(`API REST FUNCIONANDO en ${PORT}`);
        });
    }
});

//https://github.com/eagleera/TecnologiasInternet2/tree/master/api-rest