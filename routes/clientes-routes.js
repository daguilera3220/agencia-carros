'use strict'

var express = require('express');
var clientesController = require('../controllers/clientes-controller');
var ClientesRouter = express.Router();

ClientesRouter.get('/insert', clientesController.readCreateClient);
ClientesRouter.post('/insert', clientesController.createClient);
ClientesRouter.get('/list/:pg', clientesController.readClient);
ClientesRouter.get('/update/:id', clientesController.readUpdateClient);
ClientesRouter.post('/update/:id', clientesController.updateClient);
ClientesRouter.get('/delete/:id', clientesController.deleteClient);

module.exports = ClientesRouter;