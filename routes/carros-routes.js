'use strict'

var express = require('express');
var carrosController = require('../controllers/carros-controller');
var carrosRouter = express.Router();

carrosRouter.get('/insert', carrosController.readCreateCar);
carrosRouter.post('/insert', carrosController.createCar);
carrosRouter.get('/list/:pg', carrosController.readCar);
carrosRouter.get('/update/:id', carrosController.readUpdateCar);
carrosRouter.post('/update/:id', carrosController.updateCar);
carrosRouter.get('/delete/:id', carrosController.deleteCar);

module.exports = carrosRouter;