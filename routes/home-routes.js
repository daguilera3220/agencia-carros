'use strict'

var express = require('express');
var carrosController = require('../controllers/carros-controller');
var homeRouter = express.Router();

homeRouter.get('', carrosController.home);

module.exports = homeRouter;